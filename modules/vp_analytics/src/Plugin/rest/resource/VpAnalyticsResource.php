<?php

namespace Drupal\vp_analytics\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Drupal\vp_analytics\Entity\VpAnalytics;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Represents analytics records as resources.
 *
 * @RestResource (
 *   id = "vp_analytics",
 *   label = @Translation("VpAnalytics"),
 *   uri_paths = {
 *     "create" = "/api/vp/analytics",
 *     "canonical" = "/api/vp/analytics"
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
class VpAnalyticsResource extends ResourceBase {

  /**
   * The key-value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        KeyValueFactoryInterface $keyValueFactory,
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $keyValueFactory);
    $this->storage = $keyValueFactory->get('vp_analytics');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('rest'),
          $container->get('keyvalue')
      );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param array $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(array $data = []) {

    $uid = $data['uid'] ?? NULL;
    $uuid = $data['uuid'] ?? NULL;

    $headers = [
      'Access-Control-Allow-Origin' => '*',
      'Access-Control-Allow-Methods' => 'POST',
      'Access-Control-Allow-Headers' => 'Authorization',
    ];

    if (!$uuid) {
      return new ModifiedResourceResponse('Virtual patient parameter is missing', 404, $headers);
    }

    if (!$uid) {
      return new ModifiedResourceResponse('User parameter is missing', 404, $headers);
    }

    $user = User::load($uid);
    if (!$user) {
      return new ModifiedResourceResponse('User not found', 404, $headers);
    }

    $storage = \Drupal::entityTypeManager()->getStorage('virtual_patient');
    $virtual_patient = $storage->loadByProperties(['uuid' => $uuid]);
    $virtual_patient = reset($virtual_patient);
    if (!$virtual_patient) {
      return new ModifiedResourceResponse('Virtual patient not found', 404, $headers);
    }
    VpAnalytics::create(
          [
            'uid' => $user,
            'label' => $virtual_patient->label(),
            'field_user' => $user,
            'field_virtual_patient' => $virtual_patient,
          ]
      )->save();
    return new ModifiedResourceResponse($data, 200, $headers);
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseRoute($canonical_path, $method) {
    $route = parent::getBaseRoute($canonical_path, $method);
    return $route;
  }

}
