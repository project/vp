## Virtual Patient (VP) Module for Drupal

 *__Note__: This is in-progress work.*

A virtual patient is a specific type of computer-based program that simulates real-life clinical scenarios. Learners using virtual patients emulate the roles of healthcare providers to obtain a history, conduct physical exams, and make diagnostic and therapeutic decisions.

The Virtual Patient (VP) module is designed to enhance medical education and training by allowing Drupal users to create and interact with virtual patients. Whether you’re a medical student, educator, or healthcare professional, VP provides a powerful tool for simulating clinical scenarios and practicing diagnostic and therapeutic decision-making.

### Features

- Virtual Patient Creation: Users can create detailed virtual patient profiles, including medical history, symptoms, and physical examination findings.

- Mobile Player Integration (Coming Soon): The VP module will soon support a mobile player, enabling users to play through virtual patient scenarios on their mobile devices.

## Sub-Modules:

 - **vp_analytics**: Provides analytics and insights related to virtual patient interactions.

 - **vp_export**: Allows exporting virtual patient data for research or sharing purposes. *(COMING SOON)*

 - **vp_graphql**: Integrates GraphQL support for querying virtual patient data. *(COMING SOON)*

 - **vp_visual_editor**: Offers a visual editor for creating and modifying virtual patient profiles. *(COMING SOON)*

## Installation

- Install the module using the standard Drupal module installation process.

- Enable the VP module and any desired sub-modules.

## Usage

- Navigate to the Virtual Patient section in your Drupal admin panel.
- Create new virtual patients by filling in relevant details such as symptoms, medical history, and examination findings.
- Explore the various sub-modules to enhance your virtual patient experience.

## Documentation

*Coming soon*

## Contributing

We welcome contributions from the Drupal community! If you’d like to improve the VP module, report issues, or suggest enhancements, please visit our GitHub repository.

* To submit bug reports and feature suggestions, or to track changes visit: [https://www.drupal.org/project/issues/vp](https://www.drupal.org/project/issues/vp)
